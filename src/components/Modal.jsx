import {useState} from "react"
import cerrarBtn from '../img/cerrar.svg'
const Modal = ({setModal,animarModal,setAnimarModal}) => {
    const [nombre, setNombre] = useState("")
    const [cantidad, setCantidad] = useState(0)
    const ocultarModal=()=>{
        setAnimarModal(false)
        setTimeout(()=>{
            setModal(false)
        },300)
        
    }
    return (<div className="modal">
        <div className="cerrar-modal">
            <img src={cerrarBtn} alt='cerrar modal'
            onClick={ocultarModal}/>
        </div>
            <form className={`formulario ${animarModal? "animar" : 'cerrar'}`}>
                <legend>Nuevo Gasto</legend>
                <div className='campo'>
                    <label htmlFor='nombre'>Nombre Gasto</label>
                    <input id="nombre" type="text" placeholder='Añade Nombre del gasto' 
                        value={nombre} onChange={e => setNombre(e.target.vale)}
                    />
                </div>
                <div className='campo'>
                    <label htmlFor='cantidad'>Cantidad</label>
                    <input id="cantidad" type='number' placeholder='Añade la cantidad ejem: 500'
                        value={cantidad} onChange={e => setCantidad(e.target.value)}
                    />
                </div>
                <div className='campo'>
                    <label htmlFor='categiria'>Categoría</label>
                    <select id='categoria'>
                        <option value="">-- Seleccione --</option>
                        <option value="ahorro">Ahorro</option>
                        <option value="comida">Comida</option>
                        <option value="casa">Casa</option>
                        <option value="gastos">Gastos</option>
                        <option value="ocio">Ocio</option>
                        <option value="salud">Salud</option>
                        <option value="suscripciones">Suscripciones</option>
                    </select>
                </div>
            <input type='submit' value="AÑADIR GASTO"/>
            </form>
        </div>)
}
export default Modal