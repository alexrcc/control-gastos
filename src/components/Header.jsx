import {React} from 'react'
import Budget from './Budget'
import ControlBudget from './ControlBudget'

const Header=({presupuesto,
     setPresupuesto, 
     isValidPresupuesto, 
     setIsValidPresupuesto})=>{
    return(
    <header>
        <h1>Control de Gastos Personales</h1>
        {isValidPresupuesto?
        (<ControlBudget
            presupuesto = {presupuesto}
        />
        ):(
            <Budget
            presupuesto = {presupuesto}
            setPresupuesto = {setPresupuesto}
            setIsValidPresupuesto = {setIsValidPresupuesto}
        />
        )}
        
    </header>)
}
export default Header
